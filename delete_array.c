

---------- Forwarded message ---------
From: Mallikarjuna Kanakagiri <mallikarjuna.cv19@bmsce.ac.in>
Date: Sat, Nov 2, 2019 at 10:35 AM
Subject: Fwd: asd
To: Pratheek D R <pratheekdr.cv19@bmsce.ac.in>




---------- Forwarded message ---------
From: Pranav Rao H S <pranavrao.cv19@bmsce.ac.in>
Date: Sat, Nov 2, 2019 at 10:33 AM
Subject: asd
To: Prajwol Shrestha <prajwol.cv19@bmsce.ac.in>, Mallikarjuna Kanakagiri <mallikarjuna.cv19@bmsce.ac.in>


#include <stdio.h>
int main()
{
   int array[100], position, i, n;
   printf("Enter number of elements in array\n");
   scanf("%d", &n);
   printf("Enter %d elements\n", n);
   for (i=0;i<n;i++)
      scanf("%d", &array[i]);
   printf("Enter the location where you wish to delete element\n");
   scanf("%d", &position);
   if (position>=n+1)
      printf("Deletion not possible.\n");
   else
   {
      for (i=position-1;i<n-1;i++)
         array[i] = array[i+1];
 
      printf("Resultant array:\n");
 
      for (i= 0;i< n-1;i++)
         printf("%d\n", array[i]);
   }
 
   return 0;
}
